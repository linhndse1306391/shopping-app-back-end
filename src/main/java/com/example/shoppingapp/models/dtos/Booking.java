package com.example.shoppingapp.models.dtos;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Data
@Table(name = "booking")
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id", nullable = false)
    private Long bookingId;

    @Column(name = "booking_title", nullable = false)
    private String bookingTitle;

    @Column(name = "booking_total", nullable = false)
    private String bookingTotal;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "users_id", nullable = false)
    private Long usersId;

}
