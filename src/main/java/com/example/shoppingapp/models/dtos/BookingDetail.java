package com.example.shoppingapp.models.dtos;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "booking_detail")
@Data
public class BookingDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_detail_id", nullable = false)
    private Long bookingDetailId;

    @Column(name = "quantity", nullable = false)
    private String quantity;

    @Column(name = "booking_id", nullable = false)
    private Long bookingId;

    @Column(name = "product_id", nullable = false)
    private String productId;

}
