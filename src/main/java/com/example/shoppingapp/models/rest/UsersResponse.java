package com.example.shoppingapp.models.rest;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersResponse {
    private String username;
    private String name;
    private String phone;
    private String images;
    private String role;
    private String token;
}
