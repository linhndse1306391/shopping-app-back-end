package com.example.shoppingapp.ServiceIml;

import com.example.shoppingapp.models.dtos.Users;
import com.example.shoppingapp.models.rest.UsersResponse;
import com.example.shoppingapp.reponsitorys.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String usesrname) {
        // Kiểm tra xem user có tồn tại trong database không?
        Users users = usersRepository.findByUsername(usesrname);
        if (users == null) {
            throw new UsernameNotFoundException(usesrname);
        }
        return new CustomUserDetails(users);
    }


    public UserDetails loadUserById(Long userId) {
        Users users = usersRepository.findByUsersId(userId);
        if (users == null) {
            throw new UsernameNotFoundException(String.valueOf(userId));
        }
        return new CustomUserDetails(users);
    }


    public UsersResponse getUsersResponse(String usesrname, String jwt) {
        Users users = usersRepository.findByUsername(usesrname);
        return UsersResponse.builder().token(jwt).username(users.getUsername()).images(users.getImages()).name(users.getName()).phone(users.getPhone()).role(users.getRole()).build();

    }


    public boolean registrationUser(Users users) {
        boolean check = false;
        try {
            usersRepository.save(users);
            check = true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return check;
    }

    public boolean checkUsername(String username){
        boolean check = false;
        try {
            usersRepository.findByUsername(username);
            check = true;
        }catch (Exception e){

        }
        return check;
    }


    public Users getUsersByUsername(String username){
        return usersRepository.findByUsername(username);
    }
}