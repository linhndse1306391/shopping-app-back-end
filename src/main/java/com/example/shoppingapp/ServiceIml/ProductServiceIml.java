package com.example.shoppingapp.ServiceIml;

import com.example.shoppingapp.models.dtos.Product;
import com.example.shoppingapp.reponsitorys.ProductRepository;
import com.example.shoppingapp.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceIml implements ProductService{

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }
}
