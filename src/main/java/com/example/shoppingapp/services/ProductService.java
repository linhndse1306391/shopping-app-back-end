package com.example.shoppingapp.services;

import com.example.shoppingapp.models.dtos.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

}
