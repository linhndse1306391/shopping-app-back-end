package com.example.shoppingapp.reponsitorys;

import com.example.shoppingapp.models.dtos.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsersRepository extends JpaRepository<Users, Long>, JpaSpecificationExecutor<Users> {

    Users findByUsername(String username);

    Users findByUsersId(Long usersId);

}