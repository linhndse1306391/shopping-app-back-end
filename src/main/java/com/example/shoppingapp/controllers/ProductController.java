package com.example.shoppingapp.controllers;


import com.example.shoppingapp.ServiceIml.ProductServiceIml;
import com.example.shoppingapp.configs.ApiResponseList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;


@RestController
@RequestMapping("/product-management")
public class ProductController {

    @Autowired
    ProductServiceIml productServiceIml;


    @GetMapping
    @RequestMapping("/get-all")
    public ApiResponseList getAllProduct(){
        return ApiResponseList.builder().Status(200).StatusMess("OK").result((ArrayList) productServiceIml.getAllProduct()).build();
    }



}
