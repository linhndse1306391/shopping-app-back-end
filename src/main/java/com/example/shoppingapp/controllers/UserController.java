package com.example.shoppingapp.controllers;


import com.example.shoppingapp.configs.ApiResponse;
import com.example.shoppingapp.models.dtos.Users;
import com.example.shoppingapp.models.rest.UsersRequest;
import com.example.shoppingapp.models.rest.UsersResponse;
import com.example.shoppingapp.securitys.JwtTokenProvider;
import com.example.shoppingapp.ServiceIml.CustomUserDetails;
import com.example.shoppingapp.ServiceIml.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-management")
public class UserController {

    @Autowired
    public PasswordEncoder passwordEncoder;
    @Autowired
    public UserService userService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider tokenProvider;

    @GetMapping
    @RequestMapping("/ping")
    public String pong() {

        return passwordEncoder.encode("admin");
    }


    @PostMapping
    @RequestMapping("/login")
    public ApiResponse login(@RequestBody UsersRequest usersRequest) {
        String jwt = null;
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            usersRequest.getUsername(), usersRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ApiResponse.builder().Status(400).StatusMess("Bad Request").result("null").build();
        }

        return ApiResponse.builder().Status(200).StatusMess("OK").result(userService.getUsersResponse(usersRequest.getUsername(),jwt)).build();

    }




    @PostMapping
    @RequestMapping("/registration")
    public ApiResponse registration(@RequestBody Users users){
        String passwordEncode = passwordEncoder.encode(users.getPassword());
        users.setPassword(passwordEncode);
        if (userService.registrationUser(users)){
            Users usersNew = userService.getUsersByUsername(users.getUsername());
            UsersResponse usersResponse =  UsersResponse.builder().role(usersNew.getRole()).phone(usersNew.getRole()).name(usersNew.getName()).images(usersNew.getImages()).username(usersNew.getUsername()).build();
            return ApiResponse.builder().StatusMess("OK").Status(200).result(usersResponse).build();
        }else{
            if (!userService.checkUsername(users.getUsername())){
                return ApiResponse.builder().Status(500).StatusMess("Username error !").build();
            }
        }
        return ApiResponse.builder().Status(500).StatusMess("ERROR").build();
    }






}
